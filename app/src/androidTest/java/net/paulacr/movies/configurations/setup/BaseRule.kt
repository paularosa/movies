package net.paulacr.movies.configurations.setup

import okhttp3.mockwebserver.MockWebServer
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

/**
 * Created by paularosa on 25/02/18.
 */

open class BaseRule : TestRule {

    val server = MockWebServer()

    override fun apply(base: Statement, description: Description): Statement  {

        return object: Statement() {
            override fun evaluate() {
                server.start()
                HttpConfigTest.configureUrlForTesting(server)
                base.evaluate()
                server.shutdown()
            }
        }
    }


}
