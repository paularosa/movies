package net.paulacr.movies.configurations.setup

import net.paulacr.movies.network.HttpConfig
import net.paulacr.movies.network.RestApi
import okhttp3.mockwebserver.MockWebServer

/**
 * Created by paularosa on 25/02/18.
 */

object HttpConfigTest {

    fun configureUrlForTesting(server: MockWebServer) {
        val config =  HttpConfig.init((server.url("/").toString()))
        RestApi.init(config)
    }

}
