package net.paulacr.movies.configurations.responseconfig

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer

/**
 * Created by paularosa on 28/02/18.
 */
open class MockResponses(private val server: MockWebServer) {

    private val RESPONSE_CODE_SUCCESS = 200
    private val RESPONSE_CODE_AUTH_ERROR = 401
    private val RESPONSE_CODE_RESOURCE_NOT_FOUND = 404

    fun withAuthenticationError() {

        val json = "{\n" +
                "  \"status_message\": \"Invalid API key: You must be granted a valid key.\",\n" +
                "  \"success\": false,\n" +
                "  \"status_code\": 7\n" +
                "}"

        server.enqueue(MockResponse()
                .setResponseCode(RESPONSE_CODE_AUTH_ERROR)
                .setBody(json))
    }

    fun withResourceNotFoundError() {

        val json = "{\n" +
                "  \"status_message\": \"The resource you requested could not be found.\",\n" +
                "  \"status_code\": 34\n" +
                "}"

        server.enqueue(MockResponse()
                .setResponseCode(RESPONSE_CODE_RESOURCE_NOT_FOUND)
                .setBody(json))
    }

    fun withMoviesSuccess() {
        val json = "{\n" +
                "    \"results\":[\n" +
                "        {\n" +
                "            \"vote_count\":972,\n" +
                "            \"id\":391713,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":7.7,\n" +
                "            \"title\":\"Lady Bird\",\n" +
                "            \"popularity\":186.593533,\n" +
                "            \"poster_path\":\"\\/vbvHTI7vfPKsONw75g9lio38SpN.jpg\",\n" +
                "            \"original_language\":\"en\",\n" +
                "            \"original_title\":\"Lady Bird\",\n" +
                "            \"genre_ids\":[\n" +
                "                35,\n" +
                "                18\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/2ByWxoMbfE3pxztCJn5qTJ5Ui2Y.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"A California high school student plans to escape from her family and small town by going to college in New York.\",\n" +
                "            \"release_date\":\"2017-09-08\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":2688,\n" +
                "            \"id\":354912,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":7.8,\n" +
                "            \"title\":\"Coco\",\n" +
                "            \"popularity\":154.427756,\n" +
                "            \"poster_path\":\"\\/eKi8dIrr8voobbaGzDpe8w0PVbC.jpg\",\n" +
                "            \"original_language\":\"en\",\n" +
                "            \"original_title\":\"Coco\",\n" +
                "            \"genre_ids\":[\n" +
                "                12,\n" +
                "                16,\n" +
                "                35,\n" +
                "                10751\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/askg3SMvhqEl4OL52YuvdtY40Yb.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"Despite his family’s baffling generations-old ban on music, Miguel dreams of becoming an accomplished musician like his idol, Ernesto de la Cruz. Desperate to prove his talent, Miguel finds himself in the stunning and colorful Land of the Dead following a mysterious chain of events. Along the way, he meets charming trickster Hector, and together, they set off on an extraordinary journey to unlock the real story behind Miguel's family history.\",\n" +
                "            \"release_date\":\"2017-10-27\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":1723,\n" +
                "            \"id\":359940,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":8.2,\n" +
                "            \"title\":\"Three Billboards Outside Ebbing, Missouri\",\n" +
                "            \"popularity\":143.408142,\n" +
                "            \"poster_path\":\"\\/vgvw6w1CtcFkuXXn004S5wQsHRl.jpg\",\n" +
                "            \"original_language\":\"en\",\n" +
                "            \"original_title\":\"Three Billboards Outside Ebbing, Missouri\",\n" +
                "            \"genre_ids\":[\n" +
                "                80,\n" +
                "                18\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/bJLJAtGjBj836UjJZNOwgrfe5Ps.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"After seven months have passed without a culprit in her daughter's murder case, Mildred Hayes makes a bold move, painting three signs leading into her town with a controversial message directed at Bill Willoughby, the town's revered chief of police. When his second-in-command Officer Jason Dixon, an immature mother's boy with a penchant for violence, gets involved, the battle between Mildred and Ebbing's law enforcement is only exacerbated.\",\n" +
                "            \"release_date\":\"2017-11-10\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":442,\n" +
                "            \"id\":460793,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":5.9,\n" +
                "            \"title\":\"Olaf's Frozen Adventure\",\n" +
                "            \"popularity\":124.436555,\n" +
                "            \"poster_path\":\"\\/47pLZ1gr63WaciDfHCpmoiXJlVr.jpg\",\n" +
                "            \"original_language\":\"en\",\n" +
                "            \"original_title\":\"Olaf's Frozen Adventure\",\n" +
                "            \"genre_ids\":[\n" +
                "                12,\n" +
                "                16,\n" +
                "                35,\n" +
                "                10751,\n" +
                "                14,\n" +
                "                10402\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/9K4QqQZg4TVXcxBGDiVY4Aey3Rn.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"Olaf is on a mission to harness the best holiday traditions for Anna, Elsa, and Kristoff.\",\n" +
                "            \"release_date\":\"2017-10-27\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":97,\n" +
                "            \"id\":401981,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":6.6,\n" +
                "            \"title\":\"Red Sparrow\",\n" +
                "            \"popularity\":74.53087,\n" +
                "            \"poster_path\":\"\\/uZwnaMQTdwZz1kwtrrU3IOqxnDu.jpg\",\n" +
                "            \"original_language\":\"en\",\n" +
                "            \"original_title\":\"Red Sparrow\",\n" +
                "            \"genre_ids\":[\n" +
                "                9648,\n" +
                "                53\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/3lHCfeMxSlIME74nv6cyR3jiTVT.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"Prima ballerina Dominika Egorova faces a bleak and uncertain future after she suffers an injury that ends her career. She soon turns to Sparrow School, a secret intelligence service that trains exceptional young people to use their minds and bodies as weapons. Egorova emerges as the most dangerous Sparrow after completing the sadistic training process. As she comes to terms with her new abilities, Dominika meets a CIA agent who tries to convince her that he is the only person she can trust.\",\n" +
                "            \"release_date\":\"2018-03-01\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":56,\n" +
                "            \"id\":445571,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":7.4,\n" +
                "            \"title\":\"Game Night\",\n" +
                "            \"popularity\":43.726622,\n" +
                "            \"poster_path\":\"\\/1wS89vns6cseCn4UHSqj97xKEKW.jpg\",\n" +
                "            \"original_language\":\"en\",\n" +
                "            \"original_title\":\"Game Night\",\n" +
                "            \"genre_ids\":[\n" +
                "                9648,\n" +
                "                35,\n" +
                "                80,\n" +
                "                28\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/jknYekTVH1n7doNVj5wiCMKhx7u.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"A group of friends who meet regularly for game nights find themselves trying to solve a murder mystery.\",\n" +
                "            \"release_date\":\"2018-02-22\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":104,\n" +
                "            \"id\":300668,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":5.6,\n" +
                "            \"title\":\"Annihilation\",\n" +
                "            \"popularity\":40.514998,\n" +
                "            \"poster_path\":\"\\/d3qcpfNwbAMCNqWDHzPQsUYiUgS.jpg\",\n" +
                "            \"original_language\":\"en\",\n" +
                "            \"original_title\":\"Annihilation\",\n" +
                "            \"genre_ids\":[\n" +
                "                9648,\n" +
                "                12,\n" +
                "                878,\n" +
                "                53,\n" +
                "                27\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/9jbCnVw5d4OdBhsrYIMwOcxaKQa.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"A biologist signs up for a dangerous, secret expedition into a mysterious zone where the laws of nature don't apply.\",\n" +
                "            \"release_date\":\"2018-02-22\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":86,\n" +
                "            \"id\":402897,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":7.6,\n" +
                "            \"title\":\"The Death of Stalin\",\n" +
                "            \"popularity\":37.993867,\n" +
                "            \"poster_path\":\"\\/66nL9hKPerEZMOeCQuWD322Nm8g.jpg\",\n" +
                "            \"original_language\":\"en\",\n" +
                "            \"original_title\":\"The Death of Stalin\",\n" +
                "            \"genre_ids\":[\n" +
                "                35\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/3ulff7BOsTJJVdNyDIMSFzkzDBv.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"Stalin's core team of ministers tussle for control following the Russian leader's stroke in 1953.\",\n" +
                "            \"release_date\":\"2017-10-20\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":389,\n" +
                "            \"id\":339877,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":8.3,\n" +
                "            \"title\":\"Loving Vincent\",\n" +
                "            \"popularity\":25.430686,\n" +
                "            \"poster_path\":\"\\/qlrhwUDe7YOZujAYanK6ij2vbY5.jpg\",\n" +
                "            \"original_language\":\"en\",\n" +
                "            \"original_title\":\"Loving Vincent\",\n" +
                "            \"genre_ids\":[\n" +
                "                16,\n" +
                "                18\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/vRejuzhA14eu6JwY1p8xX5Dln0y.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"The film brings the paintings of Vincent van Gogh to life to tell his remarkable story. Every one of the 65,000 frames of the film is an oil-painting hand-painted by 125 professional oil-painters who travelled from all across the world to the Loving Vincent studios in Poland and Greece to be a part of the production. As remarkable as Vincent’s brilliant paintings are his passionate and ill-fated life and mysterious death.\",\n" +
                "            \"release_date\":\"2017-08-02\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":167,\n" +
                "            \"id\":413362,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":5.8,\n" +
                "            \"title\":\"Roman J. Israel, Esq.\",\n" +
                "            \"popularity\":24.263719,\n" +
                "            \"poster_path\":\"\\/8e5IGlLrVjwrlDcRtzSQkXhJFWl.jpg\",\n" +
                "            \"original_language\":\"en\",\n" +
                "            \"original_title\":\"Roman J. Israel, Esq.\",\n" +
                "            \"genre_ids\":[\n" +
                "                80,\n" +
                "                18\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/rW5gI8SEO1qYbXGwRANYrKtMiQB.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"Hard-nosed liberal lawyer Roman J. Israel has been fighting the good fight forever while others take the credit. When his partner – the firm’s frontman – has a heart attack, Israel suddenly takes on that role. He soon discovers some unsettling truths about the firm – truths that conflict with his values of helping the poor and dispossessed – and finds himself in an existential crisis that leads to extreme actions.\",\n" +
                "            \"release_date\":\"2017-11-10\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":587,\n" +
                "            \"id\":290512,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":6.5,\n" +
                "            \"title\":\"The Mountain Between Us\",\n" +
                "            \"popularity\":22.534881,\n" +
                "            \"poster_path\":\"\\/3XNfYTW4XGscI81nXMSWGsQ8cpu.jpg\",\n" +
                "            \"original_language\":\"en\",\n" +
                "            \"original_title\":\"The Mountain Between Us\",\n" +
                "            \"genre_ids\":[\n" +
                "                10749,\n" +
                "                12,\n" +
                "                18\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/pYU0fUk0G8cquQaQJQnKVz5mw95.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"Stranded after a tragic plane crash, two strangers must forge a connection to survive the extreme elements of a remote snow covered mountain. When they realize help is not coming, they embark on a perilous journey across the wilderness.\",\n" +
                "            \"release_date\":\"2017-10-05\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":3,\n" +
                "            \"id\":338970,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":0,\n" +
                "            \"title\":\"Tomb Raider\",\n" +
                "            \"popularity\":22.303463,\n" +
                "            \"poster_path\":\"\\/fpeX7q6878v3Oo6uoQvKzBzK7Ls.jpg\",\n" +
                "            \"original_language\":\"en\",\n" +
                "            \"original_title\":\"Tomb Raider\",\n" +
                "            \"genre_ids\":[\n" +
                "                28,\n" +
                "                12,\n" +
                "                14\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/xdPqNygbUXAdLfilrOSnrWoRwZd.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"Lara Croft, the fiercely independent daughter of a missing adventurer, must push herself beyond her limits when she finds herself on the island where her father disappeared.\",\n" +
                "            \"release_date\":\"2018-03-14\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":16,\n" +
                "            \"id\":381719,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":5.5,\n" +
                "            \"title\":\"Peter Rabbit\",\n" +
                "            \"popularity\":21.802551,\n" +
                "            \"poster_path\":\"\\/2yjSvEDuM3rLDng40erLsWkQRfn.jpg\",\n" +
                "            \"original_language\":\"en\",\n" +
                "            \"original_title\":\"Peter Rabbit\",\n" +
                "            \"genre_ids\":[\n" +
                "                16\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/vBIvSqbSNbABqAqP92utXzSXUhE.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"Feature adaptation of Beatrix Potter's classic tale of a rebellious rabbit trying to sneak into a farmer's vegetable garden.\",\n" +
                "            \"release_date\":\"2018-02-07\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":227,\n" +
                "            \"id\":430424,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":8.1,\n" +
                "            \"title\":\"See You Up There\",\n" +
                "            \"popularity\":20.683124,\n" +
                "            \"poster_path\":\"\\/bzlO6d5Teftw3DIwIzniuefgSPp.jpg\",\n" +
                "            \"original_language\":\"fr\",\n" +
                "            \"original_title\":\"Au revoir là-haut\",\n" +
                "            \"genre_ids\":[\n" +
                "                18\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/eThQZlfythg9YruxPvjBvrarTPt.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"November 1918. A few days before the Armistice, Édouard Péricourt saves Albert Maillard's life. These two men have nothing in common but the war. Lieutenant Pradelle, by ordering a senseless assault, destroys their lives while binding them as companions in misfortune. On the ruins of the carnage of WWI, condemned to live, the two attempt to survive. Thus, as Pradelle is about to make a fortune with the war victims' corpses, Albert and Édouard mount a monumental scam with the bereaved families' commemoration and with a nation's hero worship.\",\n" +
                "            \"release_date\":\"2017-10-25\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":529,\n" +
                "            \"id\":399035,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":5.6,\n" +
                "            \"title\":\"The Commuter\",\n" +
                "            \"popularity\":20.183331,\n" +
                "            \"poster_path\":\"\\/rDeGK6FIUfVcXmuBdEORPAGPMNg.jpg\",\n" +
                "            \"original_language\":\"en\",\n" +
                "            \"original_title\":\"The Commuter\",\n" +
                "            \"genre_ids\":[\n" +
                "                28,\n" +
                "                80,\n" +
                "                18,\n" +
                "                9648,\n" +
                "                53\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/clmYuR1t4TtKcakIOvYIPrjyxDc.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"A businessman on his daily commute home gets unwittingly caught up in a criminal conspiracy that threatens not only his life but the lives of those around him.\",\n" +
                "            \"release_date\":\"2018-01-11\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":55,\n" +
                "            \"id\":416234,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":5.7,\n" +
                "            \"title\":\"Winchester\",\n" +
                "            \"popularity\":19.412544,\n" +
                "            \"poster_path\":\"\\/tHDtskokZeO9B3JBdR1dSH0uqad.jpg\",\n" +
                "            \"original_language\":\"en\",\n" +
                "            \"original_title\":\"Winchester\",\n" +
                "            \"genre_ids\":[\n" +
                "                14,\n" +
                "                27,\n" +
                "                9648,\n" +
                "                53\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/sShIJl1G15Z7NLanMBBMtB8KRFb.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"Firearm heiress Sarah Winchester is convinced that she is haunted by the souls killed at the hands of the Winchester repeating rifle. After the sudden deaths of her husband and child, she throws herself into the construction of an enormous mansion designed to keep the evil spirits at bay. But when skeptical San Francisco psychiatrist Eric Price is dispatched to the estate to evaluate her state of mind, he discovers that her obsession may not be so insane after all.\",\n" +
                "            \"release_date\":\"2018-02-02\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":1,\n" +
                "            \"id\":407451,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":0,\n" +
                "            \"title\":\"A Wrinkle in Time\",\n" +
                "            \"popularity\":18.798502,\n" +
                "            \"poster_path\":\"\\/rSb6B7pwiZbW7In6juYEYjZ4Bsw.jpg\",\n" +
                "            \"original_language\":\"en\",\n" +
                "            \"original_title\":\"A Wrinkle in Time\",\n" +
                "            \"genre_ids\":[\n" +
                "                12,\n" +
                "                878,\n" +
                "                10751,\n" +
                "                14\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/zqUaMojohr96itJYCE3W1NauTn7.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"After the disappearance of her scientist father, three peculiar beings send Meg, her brother, and her friend to space in order to find him.\",\n" +
                "            \"release_date\":\"2018-03-08\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":2875,\n" +
                "            \"id\":295693,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":6.3,\n" +
                "            \"title\":\"The Boss Baby\",\n" +
                "            \"popularity\":18.269839,\n" +
                "            \"poster_path\":\"\\/unPB1iyEeTBcKiLg8W083rlViFH.jpg\",\n" +
                "            \"original_language\":\"en\",\n" +
                "            \"original_title\":\"The Boss Baby\",\n" +
                "            \"genre_ids\":[\n" +
                "                16,\n" +
                "                35,\n" +
                "                10751\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/8keMlLuzB9XIUBnbdEq5DCqZdHQ.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"A story about how a new baby's arrival impacts a family, told from the point of view of a delightfully unreliable narrator, a wildly imaginative 7 year old named Tim.\",\n" +
                "            \"release_date\":\"2017-03-23\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":110,\n" +
                "            \"id\":401898,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":7.1,\n" +
                "            \"title\":\"Thelma\",\n" +
                "            \"popularity\":16.439301,\n" +
                "            \"poster_path\":\"\\/yzSfG8TLAqw7nMQh2lxirQkQpgs.jpg\",\n" +
                "            \"original_language\":\"no\",\n" +
                "            \"original_title\":\"Thelma\",\n" +
                "            \"genre_ids\":[\n" +
                "                10749,\n" +
                "                18,\n" +
                "                9648\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/fSyNyntZmtNb1Bn5NNbvqR7DRsx.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"A college student starts to experience extreme seizures while studying at a university in Oslo, Norway. She soon learns that the violent episodes are a symptom of inexplicable, and often dangerous, supernatural abilities.\",\n" +
                "            \"release_date\":\"2017-09-15\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"vote_count\":165,\n" +
                "            \"id\":384680,\n" +
                "            \"video\":false,\n" +
                "            \"vote_average\":5.3,\n" +
                "            \"title\":\"Hostiles\",\n" +
                "            \"popularity\":15.578694,\n" +
                "            \"poster_path\":\"\\/rqoezyB51GfhiloOB5ZErg5HXas.jpg\",\n" +
                "            \"original_language\":\"en\",\n" +
                "            \"original_title\":\"Hostiles\",\n" +
                "            \"genre_ids\":[\n" +
                "                12,\n" +
                "                18,\n" +
                "                37\n" +
                "            ],\n" +
                "            \"backdrop_path\":\"\\/j4F5Bd1pT2KUNToejtXkxVgrBQ3.jpg\",\n" +
                "            \"adult\":false,\n" +
                "            \"overview\":\"A legendary Native American-hating Army captain nearing retirement in 1892 is given one last assignment: to escort a Cheyenne chief and his family through dangerous territory back to his Montana reservation.\",\n" +
                "            \"release_date\":\"2017-12-22\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"page\":1,\n" +
                "    \"total_results\":142,\n" +
                "    \"dates\":{\n" +
                "        \"maximum\":\"2018-03-25\",\n" +
                "        \"minimum\":\"2018-03-10\"\n" +
                "    },\n" +
                "    \"total_pages\":8\n" +
                "}"
        server.enqueue(MockResponse()
                .setResponseCode(RESPONSE_CODE_SUCCESS)
                .setBody(json))
    }

    fun withPosterResponseOk() {

        val json = "{\n" +
                "  \"id\":445571,\n" +
                "  \"backdrops\":[\n" +
                "    {\n" +
                "      \"aspect_ratio\":1.777777777777778,\n" +
                "      \"file_path\":\"/jknYekTVH1n7doNVj5wiCMKhx7u.jpg\",\n" +
                "      \"height\":720,\n" +
                "      \"iso_639_1\":null,\n" +
                "      \"vote_average\":5.312,\n" +
                "      \"vote_count\":1,\n" +
                "      \"width\":1280\n" +
                "    },\n" +
                "    {\n" +
                "      \"aspect_ratio\":1.777777777777778,\n" +
                "      \"file_path\":\"/uZIcUfotFQKxhW0RNoczhhPTSxW.jpg\",\n" +
                "      \"height\":1080,\n" +
                "      \"iso_639_1\":null,\n" +
                "      \"vote_average\":5.246,\n" +
                "      \"vote_count\":2,\n" +
                "      \"width\":1920\n" +
                "    }\n" +
                "  ],\n" +
                "  \"posters\":[\n" +
                "    {\n" +
                "      \"aspect_ratio\":0.6666666666666666,\n" +
                "      \"file_path\":\"/1wS89vns6cseCn4UHSqj97xKEKW.jpg\",\n" +
                "      \"height\":1500,\n" +
                "      \"iso_639_1\":\"en\",\n" +
                "      \"vote_average\":5.388,\n" +
                "      \"vote_count\":4,\n" +
                "      \"width\":1000\n" +
                "    },\n" +
                "    {\n" +
                "      \"aspect_ratio\":0.6666666666666666,\n" +
                "      \"file_path\":\"/cAqZ2dC3LCVOfwUMsWgxEuLyg5j.jpg\",\n" +
                "      \"height\":1500,\n" +
                "      \"iso_639_1\":\"en\",\n" +
                "      \"vote_average\":5.388,\n" +
                "      \"vote_count\":4,\n" +
                "      \"width\":1000\n" +
                "    },\n" +
                "    {\n" +
                "      \"aspect_ratio\":0.7,\n" +
                "      \"file_path\":\"/uBAFcT9ByljWQZqBnBO5kDdgHVd.jpg\",\n" +
                "      \"height\":1000,\n" +
                "      \"iso_639_1\":\"zh\",\n" +
                "      \"vote_average\":5.312,\n" +
                "      \"vote_count\":1,\n" +
                "      \"width\":700\n" +
                "    },\n" +
                "    {\n" +
                "      \"aspect_ratio\":0.6666666666666666,\n" +
                "      \"file_path\":\"/sLwS10RHdczZhinC88qPANimbts.jpg\",\n" +
                "      \"height\":1500,\n" +
                "      \"iso_639_1\":\"ru\",\n" +
                "      \"vote_average\":5.312,\n" +
                "      \"vote_count\":1,\n" +
                "      \"width\":1000\n" +
                "    },\n" +
                "    {\n" +
                "      \"aspect_ratio\":0.6666666666666666,\n" +
                "      \"file_path\":\"/418KaWWuaZzi3UPqcxutkHqyCnB.jpg\",\n" +
                "      \"height\":3000,\n" +
                "      \"iso_639_1\":\"en\",\n" +
                "      \"vote_average\":5.312,\n" +
                "      \"vote_count\":1,\n" +
                "      \"width\":2000\n" +
                "    },\n" +
                "    {\n" +
                "      \"aspect_ratio\":0.6661665416354089,\n" +
                "      \"file_path\":\"/lXUSsWKaJMtMTnqQ1DjxzEZlQf8.jpg\",\n" +
                "      \"height\":1333,\n" +
                "      \"iso_639_1\":\"en\",\n" +
                "      \"vote_average\":5.312,\n" +
                "      \"vote_count\":1,\n" +
                "      \"width\":888\n" +
                "    },\n" +
                "    {\n" +
                "      \"aspect_ratio\":0.6675977653631285,\n" +
                "      \"file_path\":\"/zm9TZFoqRtLoXuF3w2ZeL85TPZf.jpg\",\n" +
                "      \"height\":1432,\n" +
                "      \"iso_639_1\":\"en\",\n" +
                "      \"vote_average\":5.246,\n" +
                "      \"vote_count\":2,\n" +
                "      \"width\":956\n" +
                "    },\n" +
                "    {\n" +
                "      \"aspect_ratio\":0.6831767719897524,\n" +
                "      \"file_path\":\"/bzae3zh84r7ihuq1EJ2UpbT5tFD.jpg\",\n" +
                "      \"height\":1171,\n" +
                "      \"iso_639_1\":\"ru\",\n" +
                "      \"vote_average\":5.172,\n" +
                "      \"vote_count\":1,\n" +
                "      \"width\":800\n" +
                "    },\n" +
                "    {\n" +
                "      \"aspect_ratio\":0.6666666666666666,\n" +
                "      \"file_path\":\"/pTP2tQwE2sdY1Cr0rX3LAV1XMR7.jpg\",\n" +
                "      \"height\":750,\n" +
                "      \"iso_639_1\":\"pt\",\n" +
                "      \"vote_average\":0.0,\n" +
                "      \"vote_count\":0,\n" +
                "      \"width\":500\n" +
                "    },\n" +
                "    {\n" +
                "      \"aspect_ratio\":0.674,\n" +
                "      \"file_path\":\"/vhRGZ8G6UZhZU3YVvnApSGj5oKL.jpg\",\n" +
                "      \"height\":1000,\n" +
                "      \"iso_639_1\":\"en\",\n" +
                "      \"vote_average\":0.0,\n" +
                "      \"vote_count\":0,\n" +
                "      \"width\":674\n" +
                "    },\n" +
                "    {\n" +
                "      \"aspect_ratio\":0.675,\n" +
                "      \"file_path\":\"/xCW4MRm8LxnuMSbe1OzwaTsrsvf.jpg\",\n" +
                "      \"height\":1600,\n" +
                "      \"iso_639_1\":\"da\",\n" +
                "      \"vote_average\":0.0,\n" +
                "      \"vote_count\":0,\n" +
                "      \"width\":1080\n" +
                "    },\n" +
                "    {\n" +
                "      \"aspect_ratio\":0.6663493574488338,\n" +
                "      \"file_path\":\"/gP15SUH7hygoovjGnzAYuGC4kIO.jpg\",\n" +
                "      \"height\":2101,\n" +
                "      \"iso_639_1\":\"bg\",\n" +
                "      \"vote_average\":0.0,\n" +
                "      \"vote_count\":0,\n" +
                "      \"width\":1400\n" +
                "    },\n" +
                "    {\n" +
                "      \"aspect_ratio\":0.6920415224913494,\n" +
                "      \"file_path\":\"/fCa4fbjtqI4m8OYykV8WHIpewUS.jpg\",\n" +
                "      \"height\":1156,\n" +
                "      \"iso_639_1\":\"bg\",\n" +
                "      \"vote_average\":0.0,\n" +
                "      \"vote_count\":0,\n" +
                "      \"width\":800\n" +
                "    },\n" +
                "    {\n" +
                "      \"aspect_ratio\":0.6666666666666666,\n" +
                "      \"file_path\":\"/yLiHKLLQ1PH6PQePlKhXHs8TlVC.jpg\",\n" +
                "      \"height\":1080,\n" +
                "      \"iso_639_1\":\"uk\",\n" +
                "      \"vote_average\":0.0,\n" +
                "      \"vote_count\":0,\n" +
                "      \"width\":720\n" +
                "    },\n" +
                "    {\n" +
                "      \"aspect_ratio\":0.6748046875,\n" +
                "      \"file_path\":\"/jH0qqWMAQmVPRYBI8KioAcuLGXI.jpg\",\n" +
                "      \"height\":1024,\n" +
                "      \"iso_639_1\":\"hu\",\n" +
                "      \"vote_average\":0.0,\n" +
                "      \"vote_count\":0,\n" +
                "      \"width\":691\n" +
                "    },\n" +
                "    {\n" +
                "      \"aspect_ratio\":0.6666666666666666,\n" +
                "      \"file_path\":\"/6dAfEtYZGv3hbQ3o3SlAUpS6aQT.jpg\",\n" +
                "      \"height\":3000,\n" +
                "      \"iso_639_1\":\"en\",\n" +
                "      \"vote_average\":0.0,\n" +
                "      \"vote_count\":0,\n" +
                "      \"width\":2000\n" +
                "    }\n" +
                "  ]\n" +
                "}"

        server.enqueue(MockResponse()
                .setResponseCode(RESPONSE_CODE_SUCCESS)
                .setBody(json))
    }

    fun withGenresResponseOk() {

        val json = "{\n" +
                "  \"genres\": [\n" +
                "    {\n" +
                "      \"id\": 28,\n" +
                "      \"name\": \"Action\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 12,\n" +
                "      \"name\": \"Adventure\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 16,\n" +
                "      \"name\": \"Animation\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 35,\n" +
                "      \"name\": \"Comedy\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 80,\n" +
                "      \"name\": \"Crime\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 99,\n" +
                "      \"name\": \"Documentary\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 18,\n" +
                "      \"name\": \"Drama\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 10751,\n" +
                "      \"name\": \"Family\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 14,\n" +
                "      \"name\": \"Fantasy\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 36,\n" +
                "      \"name\": \"History\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 27,\n" +
                "      \"name\": \"Horror\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 10402,\n" +
                "      \"name\": \"Music\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 9648,\n" +
                "      \"name\": \"Mystery\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 10749,\n" +
                "      \"name\": \"Romance\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 878,\n" +
                "      \"name\": \"Science Fiction\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 10770,\n" +
                "      \"name\": \"TV Movie\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 53,\n" +
                "      \"name\": \"Thriller\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 10752,\n" +
                "      \"name\": \"War\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 37,\n" +
                "      \"name\": \"Western\"\n" +
                "    }\n" +
                "  ]\n" +
                "}"

        server.enqueue(MockResponse()
                .setResponseCode(RESPONSE_CODE_SUCCESS)
                .setBody(json))
    }
}