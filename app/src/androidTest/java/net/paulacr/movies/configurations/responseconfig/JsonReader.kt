package net.paulacr.movies.configurations.responseconfig

open class JsonReader {

    fun getMockJson(responseCode: Int): String {
        return ClassLoader.getSystemClassLoader()
                .getResourceAsStream("mock_error_authentication_failed.json")
                .bufferedReader()
                .readText()
    }

    fun getMockJsonSuccess(): String {
        var classLoader = ClassLoader::class.java.classLoader

        var resource = classLoader.getResourceAsStream("mock_response_movies_ok.json")

        return ClassLoader.getSystemClassLoader()
                .getResourceAsStream("mock_response_movies_ok.json")
                .bufferedReader()
                .readText()
    }

}