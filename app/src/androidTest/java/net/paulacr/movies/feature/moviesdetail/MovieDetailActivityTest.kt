package net.paulacr.movies.feature.moviesdetail

import android.content.Intent
import android.support.test.rule.ActivityTestRule
import net.paulacr.movies.configurations.responseconfig.MockResponses
import net.paulacr.movies.configurations.setup.BaseRule
import net.paulacr.movies.feature.moviesdetail.model.Genre
import net.paulacr.movies.feature.movieslist.model.Movie
import org.junit.Rule
import org.junit.Test

/**
 * Created by paularosa on 11/03/18.
 */
class MovieDetailActivityTest {

    @JvmField
    @Rule
    val rule = ActivityTestRule<MovieDetailActivity>(MovieDetailActivity::class.java,
            true, false)

    @JvmField
    @Rule
    val baseRule = BaseRule()

    private fun result(func: MovieDetailRobotsResult.() -> Unit) = MovieDetailRobotsResult().apply { func() }

    @Test
    fun validateData() {
        mockResponsesOk()
        val intent =  setupExtras()
        rule.launchActivity(intent)

        result {
            validateData(getTitle(), "Wonder woman")
            validateData(getVoteCount(), "Vote count: 5000")
            validateData(getDate(), "Release date: 2018/05")
            validateData(getOverview(),
                    "Overview: this is an overview of this movie only for testing")
        }
    }

    @Test
    fun showGenreErrorTextWhenGenresResponseFailure() {
        mockWithResponseError()
        mockPosterResponse()

        val intent =  setupExtras()
        rule.launchActivity(intent)

        result {
            validateData(getGenres(),"Genres: Couldn't get genres")
            showTextOnSnackBar("Couldn't get genres")
        }
    }

    private fun mockResponsesOk() {
        mockGenresResponse()
        mockPosterResponse()
    }

    private fun mockErrorImageResponse() {
        mockGenresResponse()
        mockWithResponseError()
    }

    private fun mockErrorGenres() {
        mockWithResponseError()
        mockPosterResponse()
    }

    private fun setupExtras(): Intent {
        val  genresId : List<String> = mutableListOf("28", "35", "80")
        val movie = Movie("12345", "Wonder woman", "2018-05-05",
                "this is an overview of this movie only for testing", "5000",
                genresId)

        val intent =  Intent()
        intent.putExtra(MovieDetailActivity.KEY_MOVIE, movie)

        return intent
    }

    private fun mockGenresResponse() {
        MockResponses(baseRule.server).withGenresResponseOk()
    }

    private fun mockPosterResponse() {
        MockResponses(baseRule.server).withPosterResponseOk()
    }

    /**
     * This mock belongs to Poster request error either genres error
     * So, if is used in both cases, have to declare it twice
     */
    private fun mockWithResponseError() {
        return MockResponses(baseRule.server).withResourceNotFoundError()
    }

}