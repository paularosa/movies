package net.paulacr.movies.feature.moviesdetail

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.ViewInteraction
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import net.paulacr.movies.R

/**
 * Created by paularosa on 11/03/18.
 */
class MovieDetailRobotsResult {

    fun validateData(viewInteraction: ViewInteraction, text: String) {
        viewInteraction.check(matches(withText(text)))
    }

    fun showTextOnSnackBar(text: String) {
        onView(withText(text)).check(matches(isDisplayed()))
    }

    fun getTitle(): ViewInteraction {
        return onView(withId(R.id.movie_title_detail))
    }

    fun getDate(): ViewInteraction {
        return onView(withId(R.id.text_release_date_detail))
    }

    fun getVoteCount(): ViewInteraction {
        return onView(withId(R.id.text_vote_count_detail))
    }

    fun getGenres(): ViewInteraction {
        return onView(withId(R.id.text_genres))
    }

    fun getOverview(): ViewInteraction {
        return onView(withId(R.id.text_overview_detail))
    }

}