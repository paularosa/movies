package net.paulacr.movies.feature.movieslist

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.scrollTo
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.v7.widget.RecyclerView
import android.view.View
import net.paulacr.movies.R
import net.paulacr.movies.matchers.CustomMatchers
import org.hamcrest.Matcher

/**
 * Created by paularosa on 07/03/18.
 */
class ListMoviesRobotsAction {

    fun scrollListToPosition(viewId: Int, position: Int) {
        onView(withId(viewId))
                .perform(RecyclerViewActions
                        .actionOnItemAtPosition<RecyclerView.ViewHolder>(position, scrollTo()))
    }

}

class ListMoviesRobotsResults {

    fun validateItemAtPosition(position: Int, text: String, viewId: Int) {

        onView(withId(R.id.movies_list))
                .check(matches(getMatcherForList(position, text, viewId)))
    }

    fun showSnackBarWithText(text: String) {
        onView(withText(text)).check(matches(isDisplayed()))
    }

    private fun getMatcherForList(position: Int, text: String, viewId: Int) : Matcher<View> {
        return CustomMatchers().atPositionOnView(position, withText(text)
                , viewId)
    }
}