package net.paulacr.movies.feature.movieslist

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import net.paulacr.movies.R
import net.paulacr.movies.configurations.responseconfig.MockResponses
import net.paulacr.movies.configurations.setup.BaseRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by paularosa on 25/02/18.
 */
@RunWith(AndroidJUnit4::class)
class ListMoviesActivityTest {

    @JvmField
    @Rule
    val rule = ActivityTestRule<ListMoviesActivity>(ListMoviesActivity::class.java,
            true, false)

    @JvmField
    @Rule
    val baseRule = BaseRule()

    private fun action(func: ListMoviesRobotsAction.() -> Unit) = ListMoviesRobotsAction().apply { func() }

    private fun result(func: ListMoviesRobotsResults.() -> Unit) = ListMoviesRobotsResults().apply { func() }

    @Test
    fun validateListItemsWithSuccess() {
        MockResponses(baseRule.server).withMoviesSuccess()
        rule.launchActivity(null)

        waitTime(1_000)
        result {
            validateItemAtPosition(0, "Lady Bird", R.id.text_grid_movie_title)
        }
    }


    @Test
    fun validateErrorAuthentication() {
        MockResponses(baseRule.server).withAuthenticationError()
        rule.launchActivity(null)

        waitTime(1_000)
        result {
            showSnackBarWithText("Error loading page")
        }
    }

    @Test
    fun validateResourceNotFound() {
        MockResponses(baseRule.server).withResourceNotFoundError()
        rule.launchActivity(null)

        waitTime(1_000)
        result {
            showSnackBarWithText("Error loading page")
        }
    }

    @Test
    fun validateScrollToEndOfList() {
        MockResponses(baseRule.server).withMoviesSuccess()
        rule.launchActivity(null)

        waitTime(1_000)
        action {
            scrollListToPosition(R.id.movies_list, 19)
        }
        result {
            validateItemAtPosition(19, "Hostiles", R.id.text_grid_movie_title)
        }
    }

    @Test
    fun validateDate() {
        MockResponses(baseRule.server).withMoviesSuccess()
        rule.launchActivity(null)

        waitTime(1_000)
        result {
            validateItemAtPosition(1, "Release date: 2017/10", R.id.text_grid_release_date)
        }
    }

    private fun waitTime(millis: Long) {
        Thread.sleep(millis)
    }

}
