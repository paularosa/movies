package net.paulacr.movies;

import android.app.Application;
import android.support.annotation.VisibleForTesting;

import com.jakewharton.threetenabp.AndroidThreeTen;

import net.paulacr.movies.network.HttpConfig;
import net.paulacr.movies.network.RestApi;

/**
 * Created by paularosa on 05/03/18.
 */

public class MoviesApplication extends Application {

    private String url = "https://api.themoviedb.org/";

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidThreeTen.init(this);
        HttpConfig config = HttpConfig.init(url);
        RestApi.init(config);

    }

    public String getUrl() {
        return url;
    }

    @VisibleForTesting
    public void setUrl(String url) {
        this.url = url;
    }
}
