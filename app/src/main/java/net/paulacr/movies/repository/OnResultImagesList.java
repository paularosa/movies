package net.paulacr.movies.repository;

import net.paulacr.movies.feature.moviesdetail.model.ImagesList;

/**
 * Created by paularosa on 08/03/18.
 */

public interface OnResultImagesList {

    void onResult(ImagesList imagesList);

    void onErrorImages();
}
