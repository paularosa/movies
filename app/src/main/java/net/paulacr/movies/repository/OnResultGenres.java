package net.paulacr.movies.repository;

import net.paulacr.movies.feature.moviesdetail.model.Genres;

/**
 * Created by paularosa on 10/03/18.
 */

public interface OnResultGenres {

    void onResultGenres(Genres genres);

    void onErrorGenres();
}
