package net.paulacr.movies.repository;

/**
 * Created by paularosa on 11/03/18.
 */

public class RepositoryAcessor {

    private Repository repository = Repository.getInstance();

    public void fetchMovies(OnResultFetchingData onResultFetchingData, String page) {
        repository.fetchMovies(onResultFetchingData, page);
    }

    public void fetchImages(String id, OnResultImagesList onResultImagesList) {
        repository.fetchImages(id, onResultImagesList);
    }

    public void fetchGenres(OnResultGenres onResultGenres) {
        repository.fetchGenres(onResultGenres);
    }





}
