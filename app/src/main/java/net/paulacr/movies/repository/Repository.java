package net.paulacr.movies.repository;

import net.paulacr.movies.feature.moviesdetail.model.Genres;
import net.paulacr.movies.feature.moviesdetail.model.ImagesList;
import net.paulacr.movies.feature.movieslist.model.Results;

/**
 * Created by paularosa on 02/03/18.
 */

class Repository {

    private OnResultFetchingData onResultFetchingData;
    private OnResultImagesList onResultImagesList;
    private OnResultGenres onResultGenres;

    private static Genres genresResponse;
    private Requester requester = new Requester();
    private static Repository instance;

    private Repository() {

    }

    synchronized static Repository getInstance() {
        if (instance == null) {
            instance = new Repository();
        }
        return instance;
    }

    private void setInterfaceMovies(OnResultFetchingData onResultFetchingData) {
        this.onResultFetchingData = onResultFetchingData;
    }

    private void setInterfaceImages(OnResultImagesList onResultImagesList) {
        this.onResultImagesList = onResultImagesList;
    }

    void fetchMovies(OnResultFetchingData onResultFetchingData, String page) {
        setInterfaceMovies(onResultFetchingData);
        requester.fetchData(page);
    }

    void fetchImages(String id, OnResultImagesList onResultImagesList) {
        setInterfaceImages(onResultImagesList);
        requester.fetchImages(id);
    }

    void setMoviesResponse(Results results) {
        onResultFetchingData.onResult(results);
    }

    void setImagesResponse(ImagesList imagesList) {
        onResultImagesList.onResult(imagesList);
    }

    void setGenresResponse(Genres genres) {
        genresResponse = genres;
        onResultGenres.onResultGenres(genres);
    }

    void setError(String errorMessage) {
        onResultFetchingData.onError(errorMessage);
    }

    void setErrorGenres() {
        onResultGenres.onErrorGenres();
    }

    void setErrorImages() {
        onResultImagesList.onErrorImages();
    }

    void fetchGenres(OnResultGenres onResultGenres) {
        this.onResultGenres = onResultGenres;
        if(genresResponse == null) {
            requester.fetchGenres();
        } else {
            onResultGenres.onResultGenres(genresResponse);
        }

    }
}