package net.paulacr.movies.repository;

import net.paulacr.movies.feature.moviesdetail.model.Genres;
import net.paulacr.movies.feature.moviesdetail.model.ImagesList;
import net.paulacr.movies.feature.movieslist.model.Results;
import net.paulacr.movies.network.RestApi;
import net.paulacr.movies.repository.Repository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by paularosa on 02/03/18.
 */

class Requester {

    void fetchData(String page) {


        Call<Results> responseMovies = RestApi.getMoviesClient()
                .getMovies("en", page);

        responseMovies.enqueue(new Callback<Results>() {
            @Override
            public void onResponse(Call<Results> call, Response<Results> response) {

                if(response.isSuccessful()) {
                    Repository.getInstance().setMoviesResponse(response.body());
                } else {
                    Repository.getInstance().setError("error loading page");
                }
            }

            @Override
            public void onFailure(Call<Results> call, Throwable t) {
                Repository.getInstance().setError(t.getMessage());
            }
        });

    }

    public void fetchImages(String movieId) {


        Call<ImagesList> responseImages = RestApi.getMoviesClient()
                .getImages(movieId);

        responseImages.enqueue(new Callback<ImagesList>() {
            @Override
            public void onResponse(Call<ImagesList> call, Response<ImagesList> response) {
                if(response.isSuccessful()) {
                    Repository.getInstance().setImagesResponse(response.body());
                } else {
                    Repository.getInstance().setErrorImages();
                }

            }

            @Override
            public void onFailure(Call<ImagesList> call, Throwable t) {
                Repository.getInstance().setErrorImages();
            }
        });

    }

    public void fetchGenres() {
        Call<Genres> responseGenres = RestApi.getMoviesClient()
                .getGenreMovie();

        responseGenres.enqueue(new Callback<Genres>() {
            @Override
            public void onResponse(Call<Genres> call, Response<Genres> response) {
                if(response.isSuccessful()) {
                    Repository.getInstance().setGenresResponse(response.body());
                } else {
                    Repository.getInstance().setErrorGenres();
                }

            }

            @Override
            public void onFailure(Call<Genres> call, Throwable t) {
                Repository.getInstance().setErrorGenres();
            }
        });

    }
}
