package net.paulacr.movies.repository;

import net.paulacr.movies.feature.movieslist.model.Results;

/**
 * Created by paularosa on 02/03/18.
 */

public interface OnResultFetchingData {

    void onResult(Results movies);

    void onError(String message);
}
