package net.paulacr.movies.util;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by paularosa on 03/03/18.
 */

public class InfiniteScrollManager extends RecyclerView.OnScrollListener {

    private int previousTotal = 0;
    private boolean loading = true;
    private int currentPage = 1;
    private LinearLayoutManager linearLayoutManager;
    private OnScrollMore listener;

    public interface OnScrollMore {

        void onScrollMorePages(int page);
    }


    public InfiniteScrollManager(LinearLayoutManager linearLayoutManager) {
        this.linearLayoutManager = linearLayoutManager;
    }

    public void setListener(OnScrollMore instance) {
        this.listener = instance;
    }


    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);

        int visibleItemCount = recyclerView.getChildCount();
        int totalItemCount = linearLayoutManager.getItemCount();
        int firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }

        int visibleThreshold = 5;
        if (!loading && (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + visibleThreshold)) {
            currentPage++;
            listener.onScrollMorePages(currentPage);
            loading = true;
        }
    }

    public void invalidatePageCount() {
        currentPage--;
    }
}
