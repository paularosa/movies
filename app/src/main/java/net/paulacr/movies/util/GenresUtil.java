package net.paulacr.movies.util;

import net.paulacr.movies.feature.moviesdetail.model.Genre;

import java.util.List;

/**
 * Created by paularosa on 10/03/18.
 */

public class GenresUtil {

    public static String getGenre(List<String> genresId, List<Genre> genres) {
        StringBuilder genresText = new StringBuilder("");
        for(String id : genresId) {
            for(Genre genre : genres) {
                if(id.equalsIgnoreCase(genre.getId())) {
                    genresText.append(genre.getGenreName());
                    genresText.append("; ");
                }
            }
        }
        return genresText.toString();
    }
}
