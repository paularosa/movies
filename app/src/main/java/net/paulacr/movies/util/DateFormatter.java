package net.paulacr.movies.util;

import org.threeten.bp.LocalDate;
import org.threeten.bp.format.DateTimeFormatter;

/**
 * Created by paularosa on 05/03/18.
 */

public class DateFormatter {

    private static final String AMERICAN_FORMAT = "yyyy/MM";
    private static final String API_FORMAT = "yyyy-MM-dd";

    public static String getDateAmericanFormat(String date) {
        return LocalDate.parse(date, getFormatterApi()).format(getFormatterAmerican());
    }

    private static DateTimeFormatter getFormatterApi() {
        return DateTimeFormatter.ofPattern(API_FORMAT);
    }

    private static DateTimeFormatter getFormatterAmerican() {
        return DateTimeFormatter.ofPattern(AMERICAN_FORMAT);
    }

}
