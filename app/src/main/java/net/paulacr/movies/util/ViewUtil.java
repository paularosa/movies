package net.paulacr.movies.util;

import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;

/**
 * Created by paularosa on 04/03/18.
 */

public class ViewUtil {

    public static final int DEFAULT_LIST_IMAGE_WIDTH = 75;
    public static final int DEFAULT_LIST_IMAGE_HEIGH = 100;

    public static SpannableStringBuilder textToBold(String completeText, int start, int end) {

        SpannableStringBuilder spannable = new SpannableStringBuilder(completeText);
        spannable.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        return spannable;
    }

    public static int getproporcionalWidth(float density, int width) {
        return getDimensionBasedOnDensity(density, width);
    }

    private static int getDimensionBasedOnDensity(float density, int dimension) {
        return (int) (dimension * density);
    }
}
