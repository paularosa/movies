package net.paulacr.movies.util;

import net.paulacr.movies.network.MoviesService;

/**
 * Created by paularosa on 07/03/18.
 */

public class UrlBuilder {

    /**
     * Build the complete url to perform request for images. It concatenates the base image url
     * with the url path of the image
     *
     * @param path attribute with path url from movie object
     * @return complete url to perform request
     */
    public static String getUrlForImage(String path) {
        StringBuilder builder = new StringBuilder(MoviesService.IMAGES_PATH);
        builder.append(path);
        return builder.toString();
    }

}
