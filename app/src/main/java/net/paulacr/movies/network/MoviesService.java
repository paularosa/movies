package net.paulacr.movies.network;

import net.paulacr.movies.feature.moviesdetail.model.Genres;
import net.paulacr.movies.feature.moviesdetail.model.ImagesList;
import net.paulacr.movies.feature.movieslist.model.Results;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by paularosa on 26/02/18.
 */

public interface MoviesService {

    String MOVIES = "3/movie/upcoming/";
    String IMAGESLIST = "3/movie/{movie_id}/images";
    String GENRES = "3/genre/movie/list";
    String IMAGES_PATH = "https://image.tmdb.org/t/p/original";

    @GET(MOVIES)
    Call<Results> getMovies(
            @Query("language") String language,
            @Query("page") String page);

    @GET(GENRES)
    Call<Genres> getGenreMovie();

    @GET(IMAGESLIST)
    Call<ImagesList> getImages(
            @Path("movie_id") String movieId);


}
