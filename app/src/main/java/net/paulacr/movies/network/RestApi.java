package net.paulacr.movies.network;

import android.util.Log;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by paularosa on 26/02/18.
 */

public class RestApi {

    private static MoviesService service;

    public static MoviesService getMoviesClient() {
        if(service == null) {

            Log.i("teste", "nulo");
        }
        return service;
    }

    public static void init(HttpConfig config){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(config.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new Interceptors().getInterceptor())
                .build();

        service = retrofit.create(MoviesService.class);
    }
}
