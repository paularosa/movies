package net.paulacr.movies.network;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by paularosa on 28/02/18.
 */

class Interceptors {

    private String api_key = "1f54bd990f1cdfb230adb312546d765d";
    private OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
    private HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();

    OkHttpClient getInterceptor() {
        this.addLog();
        addApiKey();
        return okHttpClient.build();
    }
    private void addLog() {
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        okHttpClient.addInterceptor(httpLoggingInterceptor);
    }

    private void addApiKey() {
        okHttpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();
                HttpUrl originalUrl = originalRequest.url();

                HttpUrl url = originalUrl.newBuilder()
                        .addQueryParameter("api_key", api_key)
                        .build();

                Request.Builder requestBuilder = originalRequest.newBuilder()
                        .url(url);

                Request finalRequest = requestBuilder.build();
                return chain.proceed(finalRequest);
            }
        });
    }

}
