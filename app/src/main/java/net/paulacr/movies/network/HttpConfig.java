package net.paulacr.movies.network;

/**
 * Created by paularosa on 25/02/18.
 */

public class HttpConfig {

    private static HttpConfig instance;

    final String baseUrl;

    private HttpConfig(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public static HttpConfig getInstance() {
        return instance;
    }

    public static HttpConfig init(String baseUrl) {
        instance = new HttpConfig(baseUrl);
        return instance;
    }
}
