package net.paulacr.movies.feature.moviesdetail.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by paularosa on 08/03/18.
 */

public class Poster {

    @SerializedName("aspect_ratio")
    private String aspectRatio;

    @SerializedName("file_path")
    private String filePath;

    public String getAspectRatio() {
        return aspectRatio;
    }

    public String getFilePath() {
        return filePath;
    }
}
