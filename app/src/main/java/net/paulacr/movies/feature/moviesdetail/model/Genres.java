package net.paulacr.movies.feature.moviesdetail.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.VisibleForTesting;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by paularosa on 27/02/18.
 */

public class Genres implements Parcelable{

    @SerializedName("genres")
    private List<Genre> genres;

    @VisibleForTesting
    public Genres(List<Genre> genres) {
        this.genres = genres;
    }

    protected Genres(Parcel in) {
        genres = in.createTypedArrayList(Genre.CREATOR);
    }

    public static final Creator<Genres> CREATOR = new Creator<Genres>() {
        @Override
        public Genres createFromParcel(Parcel in) {
            return new Genres(in);
        }

        @Override
        public Genres[] newArray(int size) {
            return new Genres[size];
        }
    };

    public List<Genre> getGenres() {
        return genres;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(genres);
    }
}
