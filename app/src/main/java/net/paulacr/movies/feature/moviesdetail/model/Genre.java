package net.paulacr.movies.feature.moviesdetail.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.VisibleForTesting;

import com.google.gson.annotations.SerializedName;

/**
 * Created by paularosa on 27/02/18.
 */

public class Genre implements Parcelable{

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String genreName;

    @VisibleForTesting
    public Genre(String id, String genreName) {
        this.id = id;
        this.genreName = genreName;
    }

    protected Genre(Parcel in) {
        id = in.readString();
        genreName = in.readString();
    }

    public static final Creator<Genre> CREATOR = new Creator<Genre>() {
        @Override
        public Genre createFromParcel(Parcel in) {
            return new Genre(in);
        }

        @Override
        public Genre[] newArray(int size) {
            return new Genre[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getGenreName() {
        return genreName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(genreName);
    }
}
