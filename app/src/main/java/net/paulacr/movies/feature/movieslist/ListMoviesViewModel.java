package net.paulacr.movies.feature.movieslist;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import net.paulacr.movies.feature.moviesdetail.model.Genres;
import net.paulacr.movies.feature.movieslist.model.Movie;
import net.paulacr.movies.feature.movieslist.model.Results;
import net.paulacr.movies.repository.OnResultFetchingData;
import net.paulacr.movies.repository.OnResultGenres;
import net.paulacr.movies.repository.RepositoryAcessor;

import java.util.List;

/**
 * Created by paularosa on 02/03/18.
 */

public class ListMoviesViewModel extends ViewModel implements OnResultFetchingData, OnResultGenres {

    private String totalPagesCount;
    private RepositoryAcessor repositoryAcessor;

    public ListMoviesViewModel() {
        repositoryAcessor = new RepositoryAcessor();
    }

    private MutableLiveData<List<Movie>> moviesLiveData = new MutableLiveData<>();
    private MutableLiveData<String> errorLiveData = new MutableLiveData<>();
    private MutableLiveData<Genres> genresLiveData = new MutableLiveData<>();

    public LiveData<List<Movie>> getMovies(int page) {
        String currentPage = String.valueOf(page);
        getMoviesFromRepository(currentPage);
        return moviesLiveData;
    }

    public LiveData<Genres> getGenres() {
        repositoryAcessor.fetchGenres(this);
        return genresLiveData;
    }

    public LiveData<String> getError() {
        return errorLiveData;
    }

    private void getMoviesFromRepository(String page) {
        repositoryAcessor.fetchMovies(this, page);
    }

    @Override
    public void onResult(Results movies) {
        totalPagesCount = movies.getTotalPages();
        moviesLiveData.postValue(movies.getResults());
    }

    @Override
    public void onError(String message) {
        errorLiveData.postValue(message);
    }

    @Override
    public void onResultGenres(Genres genres) {
        genresLiveData.postValue(genres);
    }

    @Override
    public void onErrorGenres() {
        //do nothing here, next screen will request
    }

    int getTotalPagesCount() {
        return Integer.parseInt(totalPagesCount);
    }
}
