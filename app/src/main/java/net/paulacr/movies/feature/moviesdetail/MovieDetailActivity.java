package net.paulacr.movies.feature.moviesdetail;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.paulacr.movies.R;
import net.paulacr.movies.feature.moviesdetail.model.Genre;
import net.paulacr.movies.feature.moviesdetail.model.Genres;
import net.paulacr.movies.feature.movieslist.ListMoviesActivity;
import net.paulacr.movies.feature.movieslist.model.Movie;
import net.paulacr.movies.util.GenresUtil;
import net.paulacr.movies.util.ViewUtil;
import net.paulacr.movies.util.UrlBuilder;

import java.util.List;

/**
 * Created by paularosa on 07/03/18.
 */

public class MovieDetailActivity extends AppCompatActivity {

    public static final String KEY_MOVIE = "key_movie";
    private static final int ERROR_GENRES = 0;

    private ImageView backdrop;
    private ImageView poster;
    private TextView textMovieTitle;
    private TextView textGenres;
    private TextView textReleaseDate;
    private TextView textVoteCount;
    private TextView textOverview;
    private View detailParent;

    private MovieDetailViewModel viewModel;
    private Movie movie;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_movie_detail);
        findViews();

        toolbar = findViewById(R.id.toolbar);
        movie = getIntent().getParcelableExtra(KEY_MOVIE);
        viewModel = ViewModelProviders.of(this).get(MovieDetailViewModel.class);

        setupToolbar();
        setDataOnViews();
        observeGenres();
        observeImages();
        observeErrors();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }


    private void findViews() {
        backdrop = findViewById(R.id.backdrop_detail);
        poster = findViewById(R.id.poster_detail);
        textMovieTitle = findViewById(R.id.movie_title_detail);
        textGenres = findViewById(R.id.text_genres);
        textReleaseDate = findViewById(R.id.text_release_date_detail);
        textVoteCount = findViewById(R.id.text_vote_count_detail);
        textOverview = findViewById(R.id.text_overview_detail);
        detailParent = findViewById(R.id.detail_parent);
    }

    private void setDataOnViews() {
        setImageBackdrop(movie.getBackdropPath());
        textMovieTitle.setText(movie.getTitle());
        textReleaseDate.setText(getReleaseDateText());
        textVoteCount.setText(getVoteCountText());
        textOverview.setText(getOverviewText());
    }

    private SpannableStringBuilder getGenresText(List<Genre> genres) {
        String genresText = getString(R.string.text_genres, GenresUtil.getGenre(movie.getGenreIds(), genres));
        return ViewUtil.textToBold(genresText, 0, 8);
    }

    private SpannableStringBuilder getGenresErrorText() {
        String genresText = getString(R.string.text_genres, getString(R.string.text_error_genres));
        return ViewUtil.textToBold(genresText, 0, 8);
    }

    private SpannableStringBuilder getReleaseDateText() {
        String releaseDate = getString(R.string.text_release_date, movie.getReleaseDate());
        return ViewUtil.textToBold(releaseDate, 0, 13);
    }

    private SpannableStringBuilder getVoteCountText() {
        String voteCount = getString(R.string.text_vote_count, movie.getVoteCount());
        return ViewUtil.textToBold(voteCount, 0, 12);
    }

    private SpannableStringBuilder getOverviewText() {
        String overviewText = getString(R.string.text_overview, movie.getOverviewDescription());
        return ViewUtil.textToBold(overviewText, 0, 9);
    }

    private void observeGenres() {

        viewModel.getGenres().observe(this, new Observer<Genres>() {
            @Override
            public void onChanged(@Nullable Genres genres) {
                assert genres != null;
                textGenres.setText(getGenresText(genres.getGenres()));
            }
        });
    }

    private void observeErrors() {
        viewModel.getError().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer errorId) {
                if(errorId == null) {
                    return;
                }

                if(errorId == ERROR_GENRES) {
                    textGenres.setText(getGenresErrorText());
                    showSnackBarErrorGenres();
                } else {
                    setImagePoster(movie.getPosterPathUrl());
                }
            }
        });
    }

    private void showSnackBarErrorGenres() {
        Snackbar.make(detailParent, getString(R.string.text_error_genres), Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.text_retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewModel.getGenres();
                    }
                })
                .show();
    }

    private void setImageBackdrop(String pathUrl) {
        Picasso.with(this)
                .load(pathUrl)
                .placeholder(R.drawable.ic_movie)
                .into(backdrop);
    }

    private void setImagePoster(String pathUrl) {
        float density = getResources().getDisplayMetrics().density;
        int width = ViewUtil.getproporcionalWidth(density, ViewUtil.DEFAULT_LIST_IMAGE_WIDTH);
        int heigh = ViewUtil.getproporcionalWidth(density, ViewUtil.DEFAULT_LIST_IMAGE_HEIGH);

        Picasso.with(this)
                .load(pathUrl)
                .resize(width, heigh)
                .placeholder(R.drawable.ic_movie)
                .into(poster);
    }

    private void observeImages() {
       viewModel.getImagePoster(movie.getId()).observe(this, new Observer<String>() {
           @Override
           public void onChanged(@Nullable String s) {
               String url = UrlBuilder.getUrlForImage(s);
               setImagePoster(url);
           }
       });
    }

    public static Intent newIntent(Context context, Movie movie) {
        Intent intent = new Intent(context, MovieDetailActivity.class);
        intent.putExtra(KEY_MOVIE , movie);
        return intent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, ListMoviesActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
