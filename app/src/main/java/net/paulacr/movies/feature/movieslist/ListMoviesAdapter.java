package net.paulacr.movies.feature.movieslist;

import android.support.v7.widget.RecyclerView;
import android.test.InstrumentationTestCase;
import android.text.SpannableStringBuilder;
import android.text.style.LineHeightSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.paulacr.movies.R;
import net.paulacr.movies.feature.movieslist.model.Movie;
import net.paulacr.movies.util.ViewUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by paularosa on 02/03/18.
 */

public class ListMoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_EMPTY = 0;
    private static final int VIEW_TYPE_ITEMS = 1;

    private List<Movie> movies = new ArrayList<>();
    private OnListItemClick onListItemClick;

    ListMoviesAdapter(List<Movie> movieList, OnListItemClick onListItemClick) {
        this.movies = movieList;
        this.onListItemClick = onListItemClick;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if(viewType == VIEW_TYPE_EMPTY) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.empty_movies, parent , false);

            return new ViewHolderEmpty(view);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_movies_list, parent, false);

            return new ViewHolderList(view);
        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        if(getItemViewType(position) == VIEW_TYPE_ITEMS) {
            final Movie movie = movies.get(position);

            ViewHolderList viewHolder = (ViewHolderList) holder;

            viewHolder.title.setText(movie.getTitle());
            viewHolder.date.setText(getReleaseText(viewHolder, movie));
            viewHolder.description.setText(getOverviewText(viewHolder, movie));

            float density = viewHolder.itemView.getResources().getDisplayMetrics().density;
            int width = ViewUtil.getproporcionalWidth(density, ViewUtil.DEFAULT_LIST_IMAGE_WIDTH);
            int heigh = ViewUtil.getproporcionalWidth(density, ViewUtil.DEFAULT_LIST_IMAGE_HEIGH);

            Picasso.with(holder.itemView.getContext())
                    .load(movie.getPosterPathUrl())
                    .placeholder(R.drawable.ic_movie)
                    .resize(width, heigh)
                    .into(viewHolder.icon);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onListItemClick.onClick(movie);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        if(movies.isEmpty()) {
            return 1;
        } else {
            return movies.size();
        }

    }

    @Override
    public int getItemViewType(int position) {
        if(movies.isEmpty()) {
            return VIEW_TYPE_EMPTY;
        } else {
            return VIEW_TYPE_ITEMS;
        }
    }

    private SpannableStringBuilder getOverviewText(ViewHolderList viewHolderList, Movie movie) {
        String overview = viewHolderList.itemView.getResources()
                .getString(R.string.text_overview, movie.getOverviewDescription());
        return ViewUtil.textToBold(overview, 0, 8);
    }

    private SpannableStringBuilder getReleaseText(ViewHolderList viewHolderList, Movie movie) {
        String release = viewHolderList.itemView.getResources()
                .getString(R.string.text_release_date, movie.getReleaseDate());

        return ViewUtil.textToBold(release, 0, 13);
    }

    static class ViewHolderList extends RecyclerView.ViewHolder {

        private ImageView icon;
        private TextView title;
        private TextView date;
        private TextView description;

        public ViewHolderList(View itemView) {
            super(itemView);

            icon = itemView.findViewById(R.id.image_grid_icon);
            title = itemView.findViewById(R.id.text_grid_movie_title);
            date = itemView.findViewById(R.id.text_grid_release_date);
            description = itemView.findViewById(R.id.text_description);
        }
    }

    static class ViewHolderEmpty extends RecyclerView.ViewHolder{


        public ViewHolderEmpty(View itemView) {
            super(itemView);
        }
    }

}
