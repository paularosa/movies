package net.paulacr.movies.feature.movieslist;

import net.paulacr.movies.feature.movieslist.model.Movie;

/**
 * Created by paularosa on 10/03/18.
 */

interface OnListItemClick {

    void onClick(Movie movie);
}
