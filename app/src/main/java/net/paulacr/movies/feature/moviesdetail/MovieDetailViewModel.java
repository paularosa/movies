package net.paulacr.movies.feature.moviesdetail;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import net.paulacr.movies.feature.moviesdetail.model.Genres;
import net.paulacr.movies.feature.moviesdetail.model.ImagesList;
import net.paulacr.movies.feature.moviesdetail.model.Poster;
import net.paulacr.movies.repository.OnResultGenres;
import net.paulacr.movies.repository.OnResultImagesList;
import net.paulacr.movies.repository.RepositoryAcessor;

import java.util.List;

/**
 * Created by paularosa on 08/03/18.
 */

public class MovieDetailViewModel extends ViewModel implements OnResultImagesList, OnResultGenres {

    private static final int ERROR_GENRES = 0;
    private static final int ERROR_IMAGES = 1;

    private MutableLiveData<String> posterLiveData = new MutableLiveData<>();
    private MutableLiveData<Genres> genresLiveData = new MutableLiveData<>();
    private MutableLiveData<Integer> error = new MutableLiveData<>();

    private RepositoryAcessor repositoryAcessor;

    public MovieDetailViewModel() {
        repositoryAcessor = new RepositoryAcessor();
    }

    public LiveData<String> getImagePoster(String movieId) {
        repositoryAcessor.fetchImages(movieId, this);
        return posterLiveData;
    }

    public LiveData<Genres> getGenres() {
        repositoryAcessor.fetchGenres(this);
        return genresLiveData;
    }

    public LiveData<Integer> getError() {
        return error;
    }

    @Override
    public void onResult(ImagesList imagesList) {
        int position;
        int listSize = getListSize(imagesList.getPosterList());

        if (listSize > 1) {
            position = getMiddlePositionList(listSize);
        } else {
            position = 0;
        }

        Log.i("item position", " " + position);
        Poster posterItem = imagesList.getPosterList().get(position);
        posterLiveData.postValue(posterItem.getFilePath());

    }

    private int getMiddlePositionList(int size) {
        return (size / 2) - 1;
    }

    private int getListSize(List<Poster> list) {
        return list.size();
    }

    @Override
    public void onErrorImages() {
        error.postValue(ERROR_IMAGES);
    }

    @Override
    public void onResultGenres(Genres genres) {
        genresLiveData.postValue(genres);
    }

    @Override
    public void onErrorGenres() {
        error.postValue(ERROR_GENRES);
    }
}
