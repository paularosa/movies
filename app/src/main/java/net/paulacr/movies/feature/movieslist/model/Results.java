package net.paulacr.movies.feature.movieslist.model;

import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.List;

/**
 * Created by paularosa on 26/02/18.
 */

public class Results {

    @SerializedName("results")
    private List<Movie>results;

    @SerializedName("total_pages")
    private String totalPages;

    public String getTotalPages() {
        return totalPages;
    }

    public List<Movie> getResults() {
        return Collections.unmodifiableList(results);
    }
}
