package net.paulacr.movies.feature.movieslist;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import net.paulacr.movies.R;
import net.paulacr.movies.feature.moviesdetail.MovieDetailActivity;
import net.paulacr.movies.feature.movieslist.model.Movie;
import net.paulacr.movies.util.InfiniteScrollManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by paularosa on 25/02/18.
 */

public class ListMoviesActivity extends AppCompatActivity implements InfiniteScrollManager.OnScrollMore, OnListItemClick {

    private RecyclerView moviesList;
    private Snackbar snackBarLoadingMovies;
    private Toolbar toolbar;

    private ListMoviesAdapter adapter;
    private InfiniteScrollManager endlessScroll;

    private List<Movie> movies = new ArrayList<>();
    private ListMoviesViewModel viewModel;


    private int currentPage = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies_list);

        toolbar = findViewById(R.id.toolbar);
        moviesList = findViewById(R.id.movies_list);
        viewModel = ViewModelProviders.of(this).get(ListMoviesViewModel.class);

        setupToolbar();
        setupRecyclerView();
        observeMoviesList();
        observeErrors();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white));
    }

    private void observeMoviesList() {

        viewModel.getMovies(currentPage).observe(this, new Observer<List<Movie>>() {
            @Override
            public void onChanged(@Nullable List<Movie> moviesList) {
                if (moviesList == null) {
                    return;
                }

                if (snackBarLoadingMovies != null && snackBarLoadingMovies.isShown()) {
                    snackBarLoadingMovies.dismiss();
                }

                movies.addAll(moviesList);
                updateAdapter();
            }
        });
    }

    private void observeErrors() {

        viewModel.getError().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String error) {
                Snackbar.make(moviesList, R.string.text_error_loading_page, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.text_retry, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                retry();
                            }
                        })
                        .show();
            }
        });
    }

    //************************************************************
    // Recyclerview Implementation *******************************
    //************************************************************

    private void retry() {
        endlessScroll.invalidatePageCount();
        viewModel.getMovies(currentPage);
    }

    private void setupRecyclerView() {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        adapter = new ListMoviesAdapter(movies, this);

        endlessScroll = new InfiniteScrollManager(manager);
        endlessScroll.setListener(this);

        moviesList.setLayoutManager(manager);
        moviesList.setAdapter(adapter);
        moviesList.addOnScrollListener(endlessScroll);
    }

    private void updateAdapter() {
        adapter.notifyItemRangeChanged(adapter.getItemCount(), movies.size() - 1);
    }

    @Override
    public void onScrollMorePages(int page) {
        currentPage = page;
        //if the page is the last one , doesn't need to perform a new request
        if(page == viewModel.getTotalPagesCount()) {
            return;
        }
        
        snackBarLoadingMovies = Snackbar.make(moviesList, R.string.text_search_movies, Snackbar.LENGTH_INDEFINITE);
        snackBarLoadingMovies.show();

        viewModel.getMovies(currentPage);
    }

    @Override
    public void onClick(Movie movie) {
        Intent intent = MovieDetailActivity.newIntent(this, movie);
        startActivity(intent);
    }
}
