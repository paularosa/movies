package net.paulacr.movies.feature.movieslist.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import net.paulacr.movies.feature.moviesdetail.model.Genre;
import net.paulacr.movies.util.DateFormatter;
import net.paulacr.movies.util.UrlBuilder;

import java.util.List;

/**
 * Created by paularosa on 26/02/18.
 */

public class Movie implements Parcelable{

    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("original_title")
    private String originalTitle;

    @SerializedName("poster_path")
    private String posterPathUrl;

    @SerializedName("genre_ids")
    private List<String> genreIds;

    @SerializedName("backdrop_path")
    private String backdropPath;

    @SerializedName("overview")
    private String overviewDescription;

    @SerializedName("release_date")
    private String releaseDate;

    @SerializedName("vote_count")
    private String voteCount;

    @SerializedName("adult")
    private boolean adult;

    @VisibleForTesting
    public Movie(String id, String title, String releaseDate, String overvivew, String voteCount,
                 List<String> genresList) {
        this.id = id;
        this.title = title;
        this.releaseDate = releaseDate;
        this.overviewDescription = overvivew;
        this.voteCount = voteCount;
        this.genreIds = genresList;
    }

    protected Movie(Parcel in) {
        id = in.readString();
        title = in.readString();
        originalTitle = in.readString();
        posterPathUrl = in.readString();
        genreIds = in.createStringArrayList();
        backdropPath = in.readString();
        overviewDescription = in.readString();
        releaseDate = in.readString();
        voteCount = in.readString();
        adult = in.readByte() != 0;
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getVoteCount() {
        return voteCount;
    }

    public boolean isAdult() {
        return adult;
    }

    public String getReleaseDate() {
        return DateFormatter.getDateAmericanFormat(releaseDate);
    }

    public String getBackdropPath() {
        Log.i("Log url", "-> " + backdropPath);
        Log.i("Log url", "-> " + UrlBuilder.getUrlForImage(backdropPath));
        return UrlBuilder.getUrlForImage(backdropPath);
    }

    public String getOverviewDescription() {
        return overviewDescription;
    }

    public String getPosterPathUrl() {
        return UrlBuilder.getUrlForImage(posterPathUrl);
    }

    public List<String> getGenreIds() {
        return genreIds;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(originalTitle);
        dest.writeString(posterPathUrl);
        dest.writeStringList(genreIds);
        dest.writeString(backdropPath);
        dest.writeString(overviewDescription);
        dest.writeString(releaseDate);
        dest.writeString(voteCount);
        dest.writeByte((byte) (adult ? 1 : 0));
    }
}
