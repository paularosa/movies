package net.paulacr.movies.feature.moviesdetail.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by paularosa on 08/03/18.
 */

public class ImagesList {

    @SerializedName("id")
    private String id;

    @SerializedName("posters")
    private List<Poster> posterList;

    public List<Poster> getPosterList() {
        return posterList;
    }
}
