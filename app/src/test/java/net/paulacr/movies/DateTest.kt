package net.paulacr.movies

import junit.framework.Assert.assertEquals
import net.paulacr.movies.util.DateFormatter
import org.junit.Test

/**
 * Created by paularosa on 07/03/18.
 */
class DateTest {

    @Test
    fun validateDateFormatTransformation() {

        val apiDate = "2017-09-08"
        val formattedDate =  DateFormatter.getDateAmericanFormat(apiDate)

        assertEquals("2017/09", formattedDate)
    }
}