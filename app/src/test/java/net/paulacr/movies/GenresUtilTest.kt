package net.paulacr.movies

import junit.framework.Assert.assertEquals
import net.paulacr.movies.feature.moviesdetail.model.Genre
import net.paulacr.movies.util.GenresUtil
import org.junit.Test

/**
 * Created by paularosa on 12/03/18.
 */
class GenresUtilTest {


    @Test
    fun getGenresText() {
        val genreAdventure = Genre("12", "adventure")
        val genreAnimation = Genre("14", "animation")
        val genreDrama = Genre("7", "drama")
        val genreFiction = Genre("8", "fiction")
        val genreRomance = Genre("1", "romance")

        val genresList : List<Genre> = mutableListOf(genreAdventure, genreAnimation, genreDrama,
                genreFiction, genreRomance)
        val  genresId : List<String> = mutableListOf("7", "8", "14")

        val genresSelected =  GenresUtil.getGenre(genresId, genresList)
        assertEquals("drama; fiction; animation; ", genresSelected)
    }
}