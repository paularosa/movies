package net.paulacr.movies

import junit.framework.Assert.assertEquals
import net.paulacr.movies.util.ViewUtil
import org.junit.Test

/**
 * Created by paularosa on 12/03/18.
 */
class ViewUtilTest {

    @Test
    fun getMeasureBasedOnDensity() {

        val density = 4.0f
        val dimension =  100

        val result = ViewUtil.getproporcionalWidth(density, 100)
        assertEquals(400, result)
    }
}