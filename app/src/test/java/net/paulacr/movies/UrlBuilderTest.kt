package net.paulacr.movies

import junit.framework.Assert.assertEquals
import net.paulacr.movies.util.UrlBuilder
import org.junit.Test

/**
 * Created by paularosa on 12/03/18.
 */
class UrlBuilderTest {
    @Test
    fun validateCompleteUrlForImages() {

        val path =  "/xyz123456sssdd.jpg"
        val completeUrl =  UrlBuilder.getUrlForImage(path)

        assertEquals("https://image.tmdb.org/t/p/original/xyz123456sssdd.jpg", completeUrl)
    }
}