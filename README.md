# Movies
An application to show the upcoming movies. Basic information about the movie is shown on this app, such as movie title, release date, genres.

## API service

This app is using TheMovieDB service to provide the upcoming movies and all its information

## Libraries used in this project

* Support Design - To provide material design components to the app
* Retrofit - to perform request easily 
* Architecture components libs - to provide an architecture organization
* Picasso - library to provide lazy loading on the image loading. With this library is possible to get the image only passing a url and a placeholder for images that are not loaded. 
* Threetenabp - Library to provide a date manager that makes easier to understand the date manipulation
* MockWebServer - a library that allows to mock request (simulating the errors) and simulate responses by providing a body method to be setted.
* Espresso - To write the instrumented tests

